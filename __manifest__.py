# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.
{
    'name': 'Partners Geolocation with Here',
    'version': '2.0',
    'category': 'Sales',
    'author': 'Jorels SAS',
    'license': 'LGPL-3',
    'website': 'https://www.jorels.com',
    'description': """
Partners Geolocation with Here
========================
    """,
    'depends': ['base',
                'base_geolocalize'],
    'data': [
        'views/res_partner_views.xml'
    ],
    'installable': True,
}
