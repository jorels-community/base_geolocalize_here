# base_geolocalize_here

Partners Geolocation with HERE, for Odoo 12
-------------------------------------------

Jorels, Jorge Sanabria

To More info please review `https:www.jorels.com`


This module is a modification made on the Odoo 12 base_geolocalize(make by Odoo S.A. under LGPL License) module

View `https://github.com/odoo/odoo/tree/12.0/addons/base_geolocalize` for more details of 'base_geolocalize'